package com.example.teum.controller;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class BoardController {

    @GetMapping({"","/"})
    public String index(){
        System.out.println("hi");
        return "index";
    }
    @GetMapping({"/login"})
    public String login(){
        System.out.println("This is Login Page");
        return "user/login";
    }
    @GetMapping({"/join"})
    public String join(){
        System.out.println("This is join Page");
        return "user/join";
    }
    @GetMapping({"/joinProc"})
    public String joinProc(){
        System.out.println("This is joinProc Page");
        return "index";
    }
}
