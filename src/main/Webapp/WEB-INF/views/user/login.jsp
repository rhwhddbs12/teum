<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="../layout/header.jsp"%>
<main>
    <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">

    <form action="/auth/loginProc" method="post">
        <%--        <img class="mb-4" src="/docs/5.0/assets/brand/bootstrap-logo.svg" alt="" width="72" height="57">--%>
        <h1 class="h3 mb-3 fw-normal">Please LogIn</h1>

        <div class="form-floating">
            <input type="text" class="form-control" id="id" name = "id"placeholder="Enter ID">
            <label for="id">ID</label>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password">
            <label for="password">Password</label>
        </div>

        <p class="mt-5 mb-3 text-muted">© 2017–2021</p>
        <button class="btn btn-primary" id="btn-login">로그인</button>
        <a href="https://kauth.kakao.com/oauth/authorize?client_id=cf52a57e5aca0ee3a0e4d4dd0d3948da&redirect_uri=http://localhost:8000/auth/kakao/callback&response_type=code "><img height="38px" src ="/image/kakao_login_button.png"></a>
    </form>
    </div>
</main>
<%@include file="../layout/footer.jsp"%>