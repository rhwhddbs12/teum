<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="layout/header.jsp"%>
<main>
    <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
        <div class="col-md-5 p-lg-5 mx-auto my-5">
            <h1 class="display-4 fw-normal">ZeroFinder</h1>
            <p class="lead fw-normal">And an even wittier subheading to boot. Jumpstart your marketing efforts with this example based on Apple’s marketing pages.</p>
            <a class="btn btn-outline-secondary" href="#">Local Search</a>
        </div>
        <div class="product-device shadow-sm d-none d-md-block"></div>
        <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
    </div>
<%-- 지도 API --%>
    <div id="map" style="width:500px;height:400px;margin: 0 auto;"></div>
    <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=	6895a2f2b5a4bd472ce07c1c69af2615"></script>
    <script>
        var container = document.getElementById('map');
        var options = {
            center: new kakao.maps.LatLng(37.626468484948745, 127.02608379089106), // 지동 중심좌표(시작 좌표)
            level: 4
        };

        var map = new kakao.maps.Map(container, options);// 지도생성
        var positions = [
            {
                content: '<div>idiom store</div>',
                latlng: new kakao.maps.LatLng(37.62560596976316, 127.02796973910606)
            },
            {
                content: '<div>미구 프로덕트</div>',
                latlng: new kakao.maps.LatLng(37.62560448584545, 127.02792784646135)
            },
            {
                content: '<div>겹</div>',
                latlng: new kakao.maps.LatLng(37.63962572964842, 127.02338972632782)
            }
        ];
        // 마커 이미지의 이미지 주소입니다

        for (var i = 0; i < positions.length; i ++) {

            // 마커를 생성합니다
            var marker = new kakao.maps.Marker({
                map: map, // 마커를 표시할 지도
                position: positions[i].latlng // 마커의 위치
            });

            // 마커에 표시할 인포윈도우를 생성합니다
            var infowindow = new kakao.maps.InfoWindow({
                content: positions[i].content // 인포윈도우에 표시할 내용
            });

            // 마커에 mouseover 이벤트와 mouseout 이벤트를 등록합니다
            // 이벤트 리스너로는 클로저를 만들어 등록합니다
            // for문에서 클로저를 만들어 주지 않으면 마지막 마커에만 이벤트가 등록됩니다
            kakao.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
            kakao.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));
        }

        // 인포윈도우를 표시하는 클로저를 만드는 함수입니다
        function makeOverListener(map, marker, infowindow) {
            return function() {
                infowindow.open(map, marker);
            };
        }

        // 인포윈도우를 닫는 클로저를 만드는 함수입니다
        function makeOutListener(infowindow) {
            return function() {
                infowindow.close();
            };
        }

    </script>



</main>
<%@include file="layout/footer.jsp"%>